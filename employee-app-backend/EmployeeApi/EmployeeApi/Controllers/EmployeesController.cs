﻿using EmployeeApi.Models;
using EmployeeApi.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeeApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeesController : Controller
    {
        IEmployeeCollectionService _employeeCollectionService;
        public EmployeesController(IEmployeeCollectionService employeeCollectionService)
        {
            _employeeCollectionService = employeeCollectionService ?? throw new ArgumentNullException(nameof(employeeCollectionService));
        }
        [HttpGet]
        public async Task<IActionResult> GetEmployees()
        {
            List<Employee> workers = await _employeeCollectionService.GetAll();
            return Ok(workers);
        }
        [HttpPost]
        public async Task<IActionResult> CreateEmployees([FromBody] Employee employee)
        {
            if (employee == null)
            {
                return BadRequest("Employee cannot be null");
            }
            bool created = await _employeeCollectionService.Create(employee);
            if (created)
            {
                return Ok(employee);
            }
            return NoContent();
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByEmployeeId(Guid id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var employee = await _employeeCollectionService.Get(id);
            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEmployee(Guid id, [FromBody] Employee employee)
        {
            if (employee == null)
            {
                return BadRequest("Employee cannot be null");
            }
            bool updated = await _employeeCollectionService.Update(id, employee);

            if (updated)
            {
                return Ok(employee);
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(Guid id)
        {
            var employee = await _employeeCollectionService.Get(id);
            if (employee == null)
            {
                return NotFound();
            }
            await _employeeCollectionService.Delete(employee.Id);

            return NoContent();
        }
    }
}
