﻿using EmployeeApi.Models;
using EmployeeApi.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LevelsController : Controller
    {
        ILevelCollectionService _levelCollectionService;

        public LevelsController(ILevelCollectionService levelCollectionService)
        {
            _levelCollectionService = levelCollectionService ?? throw new ArgumentNullException(nameof(levelCollectionService));

        }
        [HttpGet]
        public async Task<IActionResult> GeLevels()
        {
            List<Level> domains = await _levelCollectionService.GetAll();
            return Ok(domains);
        }
        [HttpPost]
        public async Task<IActionResult> CreateLevels([FromBody] Level level)
        {
            if (level == null)
            {
                return BadRequest("Level cannot be null");
            }
            bool created = await _levelCollectionService.Create(level);
            if (created)
            {
                return Ok(level);
            }
            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateLevel(Guid id, [FromBody] Level level)
        {
            if (level == null)
            {
                return BadRequest("Level cannot be null");
            }
            bool updated = await _levelCollectionService.Update(id, level);

            if (updated)
            {
                return Ok(level);
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLevel(Guid id)
        {
            var level = await _levelCollectionService.Get(id);
            if (level == null)
            {
                return NotFound();
            }
            await _levelCollectionService.Delete(level.Id);

            return NoContent();
        }
    }
}

