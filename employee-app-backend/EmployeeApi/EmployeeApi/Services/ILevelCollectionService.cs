﻿using EmployeeApi.Models;

namespace EmployeeApi.Services
{
    public interface ILevelCollectionService : ICollectionService<Level>
    {
    }
}
