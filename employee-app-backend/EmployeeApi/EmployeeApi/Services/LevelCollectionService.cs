﻿using EmployeeApi.Models;
using EmployeeApi.Settings;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeApi.Services
{
    public class LevelCollectionService : ILevelCollectionService
    {
        private readonly IMongoCollection<Level> _levels;

        public LevelCollectionService(IMongoDBSettingsL settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _levels = database.GetCollection<Level>(settings.LevelCollectionName);
        }

        public async Task<bool> Create(Level domain)
        {
            await _levels.InsertOneAsync(domain);
            return true;
        }


        public async Task<bool> Delete(Guid id)
        {
            var result = await _levels.DeleteOneAsync(level => level.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }


        public async Task<Level> Get(Guid id)
        {
            return (await _levels.FindAsync(level => level.Id == id)).FirstOrDefault();
        }


        public async Task<List<Level>> GetAll()
        {
            var result = await _levels.FindAsync(level => true);
            return result.ToList();

        }

        public async Task<bool> Update(Guid id, Level level)
        {
            level.Id = id;
            var result = await _levels.ReplaceOneAsync(level => level.Id == id, level);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _levels.InsertOneAsync(level);
                return false;
            }

            return true;
        }
    }
}
