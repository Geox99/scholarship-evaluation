﻿using EmployeeApi.Models;

namespace EmployeeApi.Services
{
    public interface IEmployeeCollectionService : ICollectionService<Employee>
    {
    }
}
