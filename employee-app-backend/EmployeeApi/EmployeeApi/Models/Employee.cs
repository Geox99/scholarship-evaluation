﻿using System;

namespace EmployeeApi.Models
{
    public class Employee
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Image { get; set; }
        public string Description { get; set; }
        public string LevelId { get; set; }

        public string ContactNumber { get; set; }

        public string DateCreated { get; set; }
    }
}
