﻿using System;

namespace EmployeeApi.Models
{
    public class Level
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
