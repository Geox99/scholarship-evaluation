﻿
namespace EmployeeApi.Settings
{
    public interface IMongoDBSettingsL
    {
        string LevelCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
