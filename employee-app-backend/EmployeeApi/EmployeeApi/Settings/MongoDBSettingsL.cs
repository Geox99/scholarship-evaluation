﻿
namespace EmployeeApi.Settings
{
    public class MongoDBSettingsL : IMongoDBSettingsL
    {
        public string LevelCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
