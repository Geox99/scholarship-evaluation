import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  @Output() emitSearchTerm=new EventEmitter<string>()
  searchedTerm:string;
  constructor() { }

  ngOnInit(): void {
  }
  searchTerm(searchedTerm:string) {
    this.emitSearchTerm.emit(searchedTerm);
    this.searchedTerm="";
  }
}
