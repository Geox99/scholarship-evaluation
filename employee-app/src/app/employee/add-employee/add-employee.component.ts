import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Level } from '../models/level';
import { EmployeeService } from '../services/employee.service';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {
  name: string;
  image:string;
  description: string;
  idLevelEmployee: string;
  contactNumber: string;
  date: Date=new Date();
  dateCreated: string=new Date().toLocaleString();
  selected:string;

  levels: Level[];
  constructor(private router: Router, private filterService: FilterService, private employeeService: EmployeeService,private datePipe: DatePipe) { }
  empname = new FormControl('', [Validators.required]);
  getErrorMessage() {
    if (this.empname.hasError('required')) {
      return 'You must enter a value';
    }

    return this.empname.hasError('empname') ? 'Not a valid name' : '';
  }
  empImage = new FormControl('', [Validators.required]);
  empDescription = new FormControl('', [Validators.required]);
  empContactNumber = new FormControl('', [Validators.required]);
  ngOnInit(): void {
    this.getFilters();
  }
  getFilters() {
    this.filterService.getFilters().subscribe((result)=>{
      this.levels=result;
    })
  }
  add() {
    switch(this.selected)
    {
      case "medium":
        this.dateCreated =this.datePipe.transform(this.dateCreated, 'medium');
        break;
      case "short":
        this.dateCreated =this.datePipe.transform(this.dateCreated, 'short');
        break;
    }
   
  this.employeeService.addEmployee(this.name,this.image,this.description,this.idLevelEmployee,this.contactNumber,this.dateCreated)
    .subscribe(()=>{this.router.navigateByUrl('')});
  }
}
