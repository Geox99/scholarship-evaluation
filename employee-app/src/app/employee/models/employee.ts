export interface Employee {
    id?: string;
    image:string;
    name?: string;
 	description?: string;
     levelId?: string;
     contactNumber?: string;
     dateCreated?:string;
}
