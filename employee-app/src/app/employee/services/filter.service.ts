import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Level } from '../models/level';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class FilterService {
  readonly baseUrl = "http://localhost:5000";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private httpClient: HttpClient) { console.log("Filter service was called");}
  getFilters(): Observable<Level[]> {
    return this.httpClient.get<Level[]>(this.baseUrl + '/Levels', this.httpOptions)
  }
}
