import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { Employee } from '../models/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  readonly baseUrl = "http://localhost:5000";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private httpClient: HttpClient) { }
  serviceCall() {
    console.log("Employee service was called");
  }
  getEmployees(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.baseUrl + '/Employees', this.httpOptions)
  }
  getFilteredEmployees(levelId: string): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.baseUrl + '/Employees', this.httpOptions)
      .pipe(
        map((employees) => employees.filter((employee) => employee.levelId === levelId)))
  }
  getFilteredEmployeesSearch(searchTerm: string): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.baseUrl+'/Employees', this.httpOptions).pipe(map((employees) => employees.filter((employee) => employee.name.toLowerCase().includes(searchTerm.toLowerCase()) || employee.description.toLowerCase().includes(searchTerm.toLowerCase()))));
  }
  getEmployeeById(id: string): Observable<Employee> {
    return this.httpClient.get<Employee>(this.baseUrl+'/Employees/'+id, this.httpOptions);
  }
  addEmployee(employeeName: string,employeeImage:string, employeeDescription: string, employeeLevelId: string,contactNumber:string,dateCreated:string): Observable<Employee> {
    let employee = {
      image:employeeImage,
      description: employeeDescription,
      name: employeeName,
      levelId: employeeLevelId,
      contactNumber: contactNumber,
      dateCreated:dateCreated
    }
    return this.httpClient.post<Employee>(this.baseUrl + "/Employees", employee, this.httpOptions);
  }
  deleteEmployee(id: string) {
    return this.httpClient.delete(this.baseUrl + "/Employees/" + id,this.httpOptions);
  }
  editEmployee(employeeId,employeeName,employeeImage, employeeDescription, employeeStatus,contactNumber) : Observable<Employee> {
    let worker = {
      id: employeeId,
      name: employeeName,
      image:employeeImage,
      description: employeeDescription,
      levelId: employeeStatus,
      contactNumber: contactNumber
      };
      return this.httpClient.put<Employee>(this.baseUrl+"/Employees/"+employeeId, worker, this.httpOptions);
  }
  sortEmployeesUp(employees: Employee[]) {
    employees.sort((a, b) => (a.name > b.name ? 1 : -1));
  }
  sortEmployeesDown(employees: Employee[]) {
    employees.sort((a, b) => (a.name < b.name ? 1 : -1));
  }
}
