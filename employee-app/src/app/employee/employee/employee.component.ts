import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../models/employee';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  @Input() selectedLevelId: string;
  @Input() searchedTerm: string;

  employees:Employee[];
  constructor(private employeeService: EmployeeService,private router:Router) { }

  ngOnInit(): void {
    this.employeeService.serviceCall();
    this.getEmployees();
    this.searchedTerm="";
  }
  ngOnChanges(): void {
    if (this.selectedLevelId) {
     // this.notes = this.noteService.getFiltredNotes(this.selectedCategoryId);
     this.employeeService.getFilteredEmployees(this.selectedLevelId).subscribe((result)=>{
       this.employees=result;
     })
    }
    if(this.searchedTerm) {
      this.employeeService.getFilteredEmployeesSearch(this.searchedTerm).subscribe((result) => {this.employees = result});
      console.log("Searched term: " + this.searchedTerm);
    }

  }
  editEmployee(id: string) {
    this.router.navigateByUrl('edit-employee/' + id);

    if(this.selectedLevelId == undefined)
      this.employeeService.getEmployees().subscribe((result) => this.employees = result);
    else
        this.employeeService.getFilteredEmployees(this.selectedLevelId).subscribe((result) => this.employees = result);
  }
  deleteEmployee(id: string){
    this.employeeService.deleteEmployee(id).subscribe(()=>this.getEmployees());
    
  }

  getEmployees()
  {
    this.employeeService.getEmployees().subscribe((result)=>{
      this.employees=result;
    })
  }
  sortAscending(employees: Employee[]) {
    this.employeeService.sortEmployeesUp(employees);
  }

  sortDescending(employees: Employee[]) {
    this.employeeService.sortEmployeesDown(employees);
  }
}
