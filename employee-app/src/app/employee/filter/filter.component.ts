import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Level } from '../models/level';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  levels:Level[];
  @Output() emitSelectedFilter = new EventEmitter<string>();
  constructor(private filterService:FilterService) { }

  ngOnInit(): void {
    this.getFilters()
  }
  getFilters() {
    this.filterService.getFilters().subscribe((result)=>{
      this.levels=result;
    })
  }
  selectFilter(levelId: string) {
    this.emitSelectedFilter.emit(levelId);
  }

}
