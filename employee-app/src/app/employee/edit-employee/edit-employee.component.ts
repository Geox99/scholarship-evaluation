import { Component, Input, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../models/employee';
import { Level } from '../models/level';
import { EmployeeService } from '../services/employee.service';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {
  @Input() employeeId: string;
  name:string
  image:string;
  description: string;
  levels: Level[];
  idLevelEmployee: string;
  id: string;
  contactNumber: String;
  employee:Employee;

  empname = new FormControl('', [Validators.required]);
  getErrorMessage() {
    if (this.empname.hasError('required')) {
      return 'You must enter a value';
    }

    return this.empname.hasError('empname') ? 'Not a valid name' : '';
  }
  empimage = new FormControl('', [Validators.required]);
  empdescription = new FormControl('', [Validators.required]);
  empContact = new FormControl('', [Validators.required]);

  constructor(private filterService: FilterService, private employeeService: EmployeeService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.employeeService.getEmployeeById(this.id).subscribe((result) => this.employee = result);
    this.getFilters();
  }
  getFilters() {
    this.filterService.getFilters().subscribe((result)=>{
      this.levels=result;
    })
  }
  edit() {
  
    this.employeeService.editEmployee(this.id, this.name,this.image, this.description, this.idLevelEmployee,this.contactNumber).subscribe(()=> {this.router.navigateByUrl('')});
   
  }
  

}
