import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  searchedTerm: string;
  levelId: string; 

  constructor() { }

  ngOnInit(): void {
  }
  receiveLevel(lvlId: string) {
    this.levelId = lvlId;
  }
  receiveSearchedTerm(searchTerm: string) {
    this.searchedTerm = searchTerm;
  }

}
