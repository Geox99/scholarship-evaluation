import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEmployeeComponent } from './employee/add-employee/add-employee.component';
import { EditEmployeeComponent } from './employee/edit-employee/edit-employee.component';
import { HomeComponent } from './employee/home/home.component';

const routes: Routes = [
  { path: "", component: HomeComponent, pathMatch:'full' },
  { path: 'edit-employee/:id', component: EditEmployeeComponent },
  {path: "addemployee", component:AddEmployeeComponent},
{ path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
